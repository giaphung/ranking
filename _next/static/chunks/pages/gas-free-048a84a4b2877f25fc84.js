_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [66], {
        "22w5": function(e, t, a) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/gas-free", function() {
                return a("tF5Y")
            }])
        },
        38: function(e, t, a) {
            a("xhzY"), e.exports = a("22w5")
        },
        tF5Y: function(e, t, a) {
            "use strict";
            a.r(t);
            a("mXGw");
            var r = a("JAph"),
                n = a.n(r),
                o = a("UutA"),
                i = a("Q5Gx"),
                c = a("z1wA"),
                s = a("ZmYT"),
                l = a("TGkK"),
                d = a("b7Z7"),
                p = a("LoMF"),
                h = a("67yl"),
                g = a("QrBS"),
                u = a("n0tG"),
                m = a("SMcu"),
                b = a("C/iq"),
                x = a("D4YM"),
                f = a("1spp"),
                j = a("xiTr"),
                w = a("lqpq"),
                O = a("zMYZ"),
                y = a("oYCi"),
                k = function() {
                    var e = Object(O.a)(),
                        t = e.slidesToShow,
                        a = e.showArrows;
                    return Object(y.jsxs)(N, {
                        alignItems: "center",
                        children: [Object(y.jsx)(u.a, {
                            className: "GasFreeLearnMore--text GasFreeLearnMore--title",
                            variant: "h2",
                            children: "Cross-blockchain resources"
                        }), Object(y.jsx)(u.a, {
                            className: "GasFreeLearnMore--text",
                            marginTop: "24px",
                            children: "Learn more about the different blockchains to start creating and selling"
                        }), Object(y.jsx)(g.a, {
                            className: "GasFreeLearnMore--carousel",
                            children: Object(y.jsx)(f.a, {
                                arrows: a,
                                dots: !0,
                                slidesToShow: t,
                                children: [{
                                    image: "article-supported-blockchain.png",
                                    description: "Which blockchains does OpenSea support?",
                                    link: "https://support.opensea.io/hc/en-us/articles/4404027708051-Which-blockchains-does-OpenSea-support-"
                                }, {
                                    image: "article-create-sell-nfts-on-polygon.png",
                                    description: "How do I create and sell NFTs on Polygon?",
                                    link: "https://support.opensea.io/hc/en-us/articles/4404029357587-How-do-I-create-and-sell-NFTs-on-Polygon-"
                                }, {
                                    image: "article-purchasing-on-polygon.png",
                                    description: "How do I purchase NFTs on Polygon?",
                                    link: "https://support.opensea.io/hc/en-us/articles/1500012889322-How-do-I-purchase-NFTs-on-Polygon-"
                                }, {
                                    image: "article-transferring-eth-to-polygon.png",
                                    description: "How do I transfer ETH from Ethereum to Polygon?",
                                    link: "https://support.opensea.io/hc/en-us/articles/1500012881642-How-do-I-transfer-ETH-from-Ethereum-to-Polygon-"
                                }, {
                                    image: "article-finding-funds-on-polygon.png",
                                    description: "How do I find my funds on Polygon?",
                                    link: "https://support.opensea.io/hc/en-us/articles/4403264773523-How-do-I-find-my-funds-on-Polygon-"
                                }, {
                                    image: "article-switch-wallet-to-polygon.png",
                                    description: "How can I switch my wallet to blockchains like Polygon?",
                                    link: "https://support.opensea.io/hc/en-us/articles/1500011368842-How-can-I-switch-my-wallet-to-blockchains-like-Polygon-"
                                }].map((function(e) {
                                    return Object(y.jsx)(j.a, {
                                        className: "GasFreeLearnMore--card",
                                        containerClassName: "GasFreeLearnMore--card-container",
                                        href: e.link,
                                        imageHeight: 200,
                                        imageUrl: "".concat(b.Sb, "/gas-free/").concat(e.image),
                                        children: Object(y.jsx)(u.a, {
                                            className: "GasFreeLearnMore--card-title",
                                            variant: "h5",
                                            children: e.description
                                        })
                                    }, e.image)
                                }))
                            })
                        }), Object(y.jsx)(g.a, {
                            marginY: "40px",
                            children: Object(y.jsx)(p.c, {
                                href: b.S,
                                children: "Learn more on our Help Center"
                            })
                        })]
                    })
                },
                N = Object(o.d)(w.a).withConfig({
                    displayName: "GasFreeLearnMorereact__Container",
                    componentId: "sc-9mbc22-0"
                })([".GasFreeLearnMore--text{margin-bottom:8px;text-align:center;width:100%;}.GasFreeLearnMore--title{font-size:32px;", "}.GasFreeLearnMore--carousel{margin:40px 0 32px 0;max-width:370px;width:90%;", " .Carousel--left-arrow{left:-8px;top:182px;}.Carousel--right-arrow{right:-18px;top:182px;}.GasFreeLearnMore--card-container{padding:0;", "}.GasFreeLearnMore--card{", "}.GasFreeLearnMore--card-title{align-items:center;display:flex;height:56px;line-height:120%;padding:10px 30px;text-align:center;}}"], Object(i.e)({
                    phoneXl: Object(o.c)(["font-size:40px;"])
                }), Object(i.e)({
                    medium: Object(o.c)(["max-width:832px;"]),
                    extraLarge: Object(o.c)(["max-width:1180px;"])
                }), Object(i.e)({
                    small: Object(o.c)(["padding:2%;margin:0 10px;"])
                }), Object(i.e)({
                    small: Object(o.c)(["width:98%;"])
                })),
                C = s.a ? "MUMBAI" : "MATIC",
                v = Object(o.d)(g.a).withConfig({
                    displayName: "GasFreePagereact__Container",
                    componentId: "sc-1fo5f2t-0"
                })(["padding:50px 0;.gas-free--main{flex-direction:column;align-items:center;width:100%;}.gas-free--h2{font-size:32px;line-height:120%;", "}.gas-free--text{line-height:26px;width:100%;}.gas-free--intro{align-items:center;flex-direction:column;justify-content:center;max-width:1280px;padding:0 40px;", " .gas-free--text{flex:1;order:1;text-align:center;", "}.gas-free--create-button{margin-top:16px;", "}}.gas-free--goodbye-gas{align-items:center;", " flex-direction:column;margin-top:80px;padding:70px 40px 0;text-align:center;width:100%;.gas-free--video-container{max-width:1280px;padding:20px 0 80px;width:100%;", "}}"], Object(i.e)({
                    phoneXl: Object(o.c)(["font-size:40px;"])
                }), Object(i.e)({
                    extraLarge: Object(o.c)(["flex-direction:row;"])
                }), Object(i.e)({
                    extraLarge: Object(o.c)(["margin-right:100px;order:0;text-align:left;"])
                }), Object(i.e)({
                    extraLarge: Object(o.c)(["align-self:flex-start;"])
                }), (function(e) {
                    var t = e.theme;
                    return Object(x.b)({
                        variants: {
                            light: {
                                "background-color": t.colors.lightMarina
                            },
                            dark: {
                                "background-color": t.colors.onyx
                            }
                        }
                    })
                }), Object(i.e)({
                    medium: Object(o.c)(["background:url(", ") center / contain no-repeat;padding:40px 100px 80px;"], b.jc)
                }));
            t.default = function() {
                return Object(y.jsx)(l.a, {
                    title: Object(m.b)("Gas-Free Marketplace"),
                    children: Object(y.jsx)(v, {
                        children: Object(y.jsxs)(g.a, {
                            className: "gas-free--main",
                            children: [Object(y.jsxs)(g.a, {
                                className: "gas-free--intro",
                                children: [Object(y.jsxs)(h.a, {
                                    className: "gas-free--text gas-free--intro-text",
                                    children: [Object(y.jsx)(u.a, {
                                        as: "h1",
                                        className: "gas-free--title gas-free--h2",
                                        variant: "h2",
                                        children: "Imagine an NFT\xa0marketplace that's gas-free."
                                    }), Object(y.jsxs)(u.a, {
                                        children: ["While we're proud to be the first and largest NFT marketplace, we're even prouder to be the first to provide cross-blockchain support, starting with a gas-free marketplace on the Polygon blockchain.", Object(y.jsx)("br", {}), Object(y.jsx)("br", {}), "That's right! Buyers no longer have to pay blockchain fees when making trades on OpenSea, and creators can fully earn their way into crypto for the first time."]
                                    }), Object(y.jsx)(p.c, {
                                        className: "gas-free--create-button",
                                        href: "/asset/create?chain=".concat(C),
                                        variant: "primary",
                                        children: "Start creating"
                                    })]
                                }), Object(y.jsx)(n.a, {
                                    alt: "",
                                    height: "478",
                                    priority: !0,
                                    src: "".concat(b.Sb, "/gas-free/gas-pump.svg"),
                                    unoptimized: !0,
                                    width: "527"
                                })]
                            }), Object(y.jsxs)(g.a, {
                                className: "gas-free--goodbye-gas",
                                children: [Object(y.jsxs)(d.a, {
                                    className: "gas-free--text",
                                    children: [Object(y.jsx)(u.a, {
                                        className: "gas-free--h2",
                                        variant: "h2",
                                        children: "Goodbye gas fees. Hello gas-free"
                                    }), Object(y.jsx)(u.a, {
                                        marginTop: "4px",
                                        children: "From creating, selling, and selling \u2014 it\u2019s all gas-free"
                                    })]
                                }), Object(y.jsx)(d.a, {
                                    className: "gas-free--video-container",
                                    children: Object(y.jsx)(c.a, {
                                        url: "https://www.youtube.com/embed/rHPaG-cflBw"
                                    })
                                })]
                            }), Object(y.jsx)(d.a, {
                                marginTop: "40px",
                                maxWidth: "1280px",
                                paddingX: "40px",
                                width: "100%",
                                children: Object(y.jsx)(k, {})
                            })]
                        })
                    })
                })
            }
        },
        xiTr: function(e, t, a) {
            "use strict";
            a("mXGw");
            var r = a("UutA"),
                n = a("b7Z7"),
                o = a("QrBS"),
                i = a("u6YR"),
                c = a("uMSw"),
                s = a("qymy"),
                l = a("oYCi");
            t.a = function(e) {
                var t = e.imageUrl,
                    a = e.href,
                    r = e.imageWidth,
                    n = e.imageHeight,
                    p = e.containerClassName,
                    h = e.contentClassName,
                    g = e.className,
                    u = e.children,
                    m = e.eventSource,
                    b = e.alt;
                return Object(l.jsx)(d, {
                    className: p,
                    children: Object(l.jsxs)(s.a, {
                        className: Object(i.a)("CarouselCard", {
                            main: !0
                        }, g),
                        eventSource: m,
                        href: a,
                        children: [Object(l.jsx)(c.a, {
                            alt: b,
                            className: "CarouselCard--image",
                            height: n,
                            sizing: "cover",
                            url: t,
                            width: r
                        }), Object(l.jsx)(o.a, {
                            className: Object(i.a)("CarouselCard", {
                                content: !0
                            }, h),
                            children: u
                        })]
                    })
                })
            };
            var d = Object(r.d)(n.a).withConfig({
                displayName: "CarouselCardreact__Container",
                componentId: "sc-152cap8-0"
            })(["display:inline-block;width:100%;.CarouselCard--main{display:inline-block;border:1px solid ", ";background-color:", ";border-radius:", ";cursor:pointer;width:100%;&:hover{box-shadow:", ";transition:0.1s;}.CarouselCard--image{border-radius:inherit;border-bottom-left-radius:0;border-bottom-right-radius:0;border-bottom:1px solid ", ";background-color:", ";}.CarouselCard--content{flex-direction:column;padding:10px;}}"], (function(e) {
                return e.theme.colors.border
            }), (function(e) {
                return e.theme.colors.card
            }), (function(e) {
                return e.theme.borderRadius.default
            }), (function(e) {
                return e.theme.shadows.default
            }), (function(e) {
                return e.theme.colors.border
            }), (function(e) {
                return e.theme.colors.border
            }))
        },
        z1wA: function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return c
            }));
            a("mXGw");
            var r = a("UutA"),
                n = a("b7Z7"),
                o = a("B6yL"),
                i = a("oYCi"),
                c = function(e) {
                    var t = e.url,
                        a = e.autoplay,
                        r = e.loop,
                        n = e.showControls,
                        c = e.title;
                    if (!Object(o.k)(t)) return null;
                    var l = t.replace("youtu.be/", "www.youtube.com/watch?v=").replace(/\/(watch\?v=|embed\/)([A-Za-z0-9_-]+\??)/, "/embed/$2?playlist=$2"),
                        d = new URLSearchParams({
                            autoplay: a ? "1" : "0",
                            controls: n ? "1" : "0",
                            loop: r ? "1" : "0",
                            modestbranding: "1",
                            rel: "0"
                        }),
                        p = "".concat(l, "&").concat(d);
                    return Object(i.jsx)(s, {
                        children: Object(i.jsx)("iframe", {
                            allow: "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
                            allowFullScreen: !0,
                            frameBorder: "0",
                            height: "315",
                            sandbox: "allow-popups allow-same-origin allow-scripts allow-presentation",
                            src: p,
                            title: c,
                            width: "560"
                        })
                    })
                },
                s = Object(r.d)(n.a).withConfig({
                    displayName: "YouTubeVideoreact__Container",
                    componentId: "sc-1e7ikaq-0"
                })(["height:0;padding-bottom:56.25%;position:relative;iframe,object,embed{height:100%;left:0;position:absolute;top:0;width:100%;}"])
        },
        zMYZ: function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return o
            }));
            var r = a("K4Ra"),
                n = a("Q5Gx"),
                o = function() {
                    var e = Object(r.a)(Object(n.d)(n.a.extraLarge), !0),
                        t = Object(r.a)(Object(n.c)(n.a.medium), !1);
                    return {
                        slidesToShow: t ? 1 : e ? 3 : 2,
                        showArrows: !t
                    }
                }
        }
    },
    [
        [38, 2, 1, 6, 4, 3, 7, 9, 0, 5, 8, 10, 12]
    ]
]);
//# sourceMappingURL=gas-free-048a84a4b2877f25fc84.js.map