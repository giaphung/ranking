_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [72], {
        44: function(e, n, t) {
            t("xhzY"), e.exports = t("bXFq")
        },
        GWLh: function(e, n, t) {
            "use strict";
            t.d(n, "a", (function() {
                return P
            }));
            t("mXGw");
            var a, r, i = t("aXrf"),
                o = t("UutA"),
                l = t("+n/q"),
                s = t("b7Z7"),
                c = t("QrBS"),
                d = t("9E9p"),
                u = t("QCNz"),
                m = t("1p8O"),
                h = t("n0tG"),
                p = t("YTPJ"),
                b = t("7v7j"),
                g = t("xiTr"),
                f = t("uMSw"),
                j = t("Q5Gx"),
                x = t("67yl"),
                y = t("ocrj"),
                O = t("t3V9"),
                w = t("m5he"),
                v = t("oYCi"),
                C = o.d.div.withConfig({
                    displayName: "CollectionCardContextMenu__ItemContainer",
                    componentId: "sc-1267gkc-0"
                })(["opacity:0.85;:hover{opacity:1;}"]),
                k = Object(o.d)(x.a).withConfig({
                    displayName: "CollectionCardContextMenu__Container",
                    componentId: "sc-1267gkc-1"
                })(["background-color:", ";border-radius:", ";color:", ";overflow:hidden;opacity:0.9;padding:4px;&:hover{box-shadow:", ";opacity:1;}"], (function(e) {
                    return e.theme.colors.card
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.text.body
                }), (function(e) {
                    return e.theme.shadows.default
                })),
                N = function(e) {
                    var n = e.dataKey,
                        r = Object(i.useFragment)(void 0 !== a ? a : a = t("hc1Z"), n);
                    return Object(v.jsx)(c.a, {
                        position: "absolute",
                        right: "12px",
                        top: "12px",
                        children: Object(v.jsx)(y.a, {
                            content: function(e) {
                                var n = e.List,
                                    t = e.Item;
                                return Object(v.jsxs)(n, {
                                    children: [Object(v.jsx)(C, {
                                        children: Object(v.jsxs)(t, {
                                            href: Object(p.b)(r),
                                            children: [Object(v.jsx)(t.Avatar, {
                                                icon: "edit"
                                            }), Object(v.jsx)(t.Content, {
                                                children: Object(v.jsx)(t.Title, {
                                                    children: "Edit"
                                                })
                                            })]
                                        })
                                    }), Object(v.jsx)(C, {
                                        children: Object(v.jsxs)(t, {
                                            href: Object(p.c)(r),
                                            children: [Object(v.jsx)(t.Avatar, {
                                                icon: "view_list"
                                            }), Object(v.jsx)(t.Content, {
                                                children: Object(v.jsx)(t.Title, {
                                                    children: "Royalties"
                                                })
                                            })]
                                        })
                                    })]
                                })
                            },
                            placement: "bottom-end",
                            children: Object(v.jsx)(k, {
                                children: Object(v.jsx)(O.a, {
                                    onClick: function(e) {
                                        e.stopPropagation(), e.preventDefault()
                                    },
                                    children: Object(v.jsx)(w.a, {
                                        "aria-label": "More",
                                        value: "more_vert"
                                    })
                                })
                            })
                        })
                    })
                },
                F = t("NXiZ"),
                P = Object.assign((function(e) {
                    var n = e.containerClassName,
                        a = e.dataKey,
                        o = e.requireBannerImage,
                        d = e.showContextMenu,
                        m = e.showTotalSupply,
                        g = Object(i.useFragment)(void 0 !== r ? r : r = t("bmVz"), a),
                        j = g.owner,
                        x = g.banner,
                        y = g.logo,
                        O = g.slug,
                        w = g.name,
                        C = g.description,
                        k = g.isVerified,
                        P = g.stats.totalSupply;
                    return !!y && (!o || !!x) ? Object(v.jsxs)(S, {
                        alt: "",
                        containerClassName: n,
                        contentClassName: "CollectionCard--content",
                        href: Object(p.e)(g),
                        imageHeight: 200,
                        imageUrl: x,
                        children: [Object(v.jsx)(f.a, {
                            alt: "",
                            className: "CollectionCard--round-image",
                            size: 50,
                            sizing: "cover",
                            url: y,
                            variant: "round"
                        }), d && Object(v.jsx)("div", {
                            className: "CollectionCard--context-menu",
                            children: Object(v.jsx)(N, {
                                dataKey: g
                            })
                        }), Object(v.jsxs)(c.a, {
                            alignItems: "center",
                            marginTop: "8px",
                            children: [Object(v.jsx)(h.a, {
                                as: "div",
                                className: "CollectionCard--name",
                                variant: "h4",
                                children: w
                            }), k && Object(v.jsx)(F.a, {
                                size: "small",
                                verificationStatus: "verified"
                            })]
                        }), j ? Object(v.jsxs)(c.a, {
                            alignItems: "center",
                            fontSize: "14px",
                            fontWeight: "500",
                            children: [Object(v.jsx)(h.a, {
                                margin: "0 4px 0 0",
                                variant: "small",
                                children: "by"
                            }), Object(v.jsx)(l.a, {
                                dataKey: j,
                                variant: "no-image"
                            })]
                        }) : Object(v.jsx)(s.a, {
                            height: "18px"
                        }), Object(v.jsx)(h.a, {
                            as: "span",
                            className: "CollectionCard--description",
                            variant: "body",
                            children: Object(v.jsx)(u.a, {
                                children: C ? "".concat(C.substring(0, 100)).concat(C.length > 100 ? "..." : "") : "Explore the ".concat(w, " collection")
                            })
                        }), m && Object(v.jsx)(h.a, {
                            className: "CollectionCard--item-count",
                            variant: "small",
                            children: "".concat(P, " ").concat(Object(b.l)("item", P))
                        })]
                    }, O) : null
                }), {
                    Skeleton: function(e) {
                        var n = e.className;
                        return Object(v.jsx)(_, {
                            className: n,
                            children: Object(v.jsxs)(m.a, {
                                children: [Object(v.jsxs)(m.a.Row, {
                                    className: "CollectionCard--skeleton-image",
                                    children: [Object(v.jsx)(m.a.Block, {
                                        height: "200px",
                                        width: "150px"
                                    }), Object(v.jsx)(m.a.Block, {
                                        direction: "rtl",
                                        height: "200px",
                                        width: "150px"
                                    })]
                                }), Object(v.jsxs)(m.a.Row, {
                                    className: "CollectionCard--skeleton-info",
                                    children: [Object(v.jsx)(m.a.Circle, {
                                        className: "CollectionCard--skeleton-circle",
                                        height: "50px",
                                        variant: "full",
                                        width: "50px"
                                    }), Object(v.jsxs)(d.a, {
                                        className: "CollectionCard--skeleton-item",
                                        height: "190px",
                                        children: [Object(v.jsx)(m.a.Block, {
                                            className: "CollectionCard--skeleton-title"
                                        }), Object(v.jsx)(m.a.Block, {
                                            className: "CollectionCard--skeleton-subtitle"
                                        }), Object(v.jsx)(m.a.Block, {
                                            className: "CollectionCard--skeleton-text"
                                        }), Object(v.jsx)(m.a.Block, {
                                            className: "CollectionCard--skeleton-text"
                                        }), Object(v.jsx)(m.a.Block, {
                                            className: "CollectionCard--skeleton-text"
                                        })]
                                    })]
                                })]
                            })
                        })
                    }
                }),
                S = Object(o.d)(g.a).withConfig({
                    displayName: "CollectionCardreact__Card",
                    componentId: "sc-1b2ne4j-0"
                })(["position:relative;.CollectionCard--content{align-items:center;margin-top:-36px;}.CollectionCard--context-menu{display:block;", "}.CollectionCard--round-image{background-color:", ";border:3px solid ", ";box-shadow:rgb(14 14 14 / 60%) 0px 0px 2px 0px;}.CollectionCard--name{color:", ";font-weight:600;font-size:16px;text-transform:none;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;text-align:center;max-width:200px;margin-top:2px;}.CollectionCard--description{max-width:80%;margin:20px 0;height:66px;overflow:hidden;text-align:center;display:-webkit-box;-webkit-line-clamp:3;-webkit-box-orient:vertical;p,ul,ol,dl,dd,blockquote,h1,h2,h3,h4,h5,h6{margin:0;}}.CollectionCard--item-count{margin-top:0;}&:hover{.CollectionCard--context-menu{display:block;}}"], Object(j.e)({
                    tabletS: Object(o.c)(["display:none;"])
                }), (function(e) {
                    return e.theme.colors.surface
                }), (function(e) {
                    return e.theme.colors.surface
                }), (function(e) {
                    return e.theme.colors.text.heading
                })),
                _ = Object(o.d)(s.a).withConfig({
                    displayName: "CollectionCardreact__SkeletonCard",
                    componentId: "sc-1b2ne4j-1"
                })(["width:100%;.CollectionCard--skeleton-image{margin-bottom:-10px;border-top-left-radius:", ";border-top-right-radius:", ";width:100%;}.CollectionCard--skeleton-info{flex-direction:column;align-items:center;margin-bottom:40px;", " .CollectionCard--skeleton-circle{margin-top:-25px;z-index:2;}.CollectionCard--skeleton-item{margin-top:-25px;flex-direction:column;align-items:center;border-radius:", ";.CollectionCard--skeleton-title{height:24px;width:75px;margin-top:20px;border-radius:12px;}.CollectionCard--skeleton-subtitle{height:16px;width:150px;margin-top:8px;margin-bottom:10px;border-radius:8px;}.CollectionCard--skeleton-text{height:12px;width:60%;margin-top:10px;}}}"], (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), Object(j.e)({
                    tabletS: Object(o.c)(["margin-bottom:0;"])
                }), (function(e) {
                    return e.theme.borderRadius.default
                }))
        },
        OsKK: function(e, n, t) {
            "use strict";
            t.d(n, "d", (function() {
                return c
            })), t.d(n, "c", (function() {
                return u
            })), t.d(n, "a", (function() {
                return m
            })), t.d(n, "b", (function() {
                return h
            }));
            var a = t("mXGw"),
                r = t.n(a),
                i = t("UutA"),
                o = t("b7Z7"),
                l = t("oYCi"),
                s = Object(i.d)(o.a).attrs((function(e) {
                    var n;
                    return {
                        as: null !== (n = e.as) && void 0 !== n ? n : "section"
                    }
                })).withConfig({
                    displayName: "Framereact__Frame",
                    componentId: "sc-139h1ex-0"
                })(["border-radius:", ";border:1px solid ", ";overflow:hidden;"], (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.border
                })),
                c = Object(i.d)(s).withConfig({
                    displayName: "Framereact__InputFrame",
                    componentId: "sc-139h1ex-1"
                })([":focus-within{border-color:", ";}"], (function(e) {
                    return e.theme.colors.seaBlue
                }));
            n.e = s;
            var d = r.a.createContext({}),
                u = Object(a.forwardRef)((function(e, n) {
                    var t = e.children,
                        a = e.className;
                    return Object(l.jsx)(d.Provider, {
                        value: {
                            isFramed: !0
                        },
                        children: Object(l.jsx)("div", {
                            className: a,
                            ref: n,
                            children: t
                        })
                    })
                })),
                m = function(e) {
                    var n = e.children,
                        t = e.className;
                    return Object(l.jsx)(d.Provider, {
                        value: {
                            isFramed: !1
                        },
                        children: Object(l.jsx)("div", {
                            className: t,
                            children: n
                        })
                    })
                },
                h = d.Consumer
        },
        Ws2v: function(e, n, t) {
            "use strict";
            t.r(n);
            var a = function() {
                var e = [{
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "collections"
                    }],
                    n = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    };
                return {
                    fragment: {
                        argumentDefinitions: e,
                        kind: "Fragment",
                        metadata: null,
                        name: "PartnershipPageQuery",
                        selections: [{
                            args: null,
                            kind: "FragmentSpread",
                            name: "WhoPartneredWithUs_data"
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: e,
                        kind: "Operation",
                        name: "PartnershipPageQuery",
                        selections: [{
                            alias: null,
                            args: [{
                                kind: "Variable",
                                name: "collections",
                                variableName: "collections"
                            }, {
                                kind: "Literal",
                                name: "first",
                                value: 14
                            }],
                            concreteType: "CollectionTypeConnection",
                            kind: "LinkedField",
                            name: "collections",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "CollectionTypeEdge",
                                kind: "LinkedField",
                                name: "edges",
                                plural: !0,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "CollectionType",
                                    kind: "LinkedField",
                                    name: "node",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "slug",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "description",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "name",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "shortDescription",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "logo",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "banner",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "isVerified",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AccountType",
                                        kind: "LinkedField",
                                        name: "owner",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "address",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "config",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "isCompromised",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            concreteType: "UserType",
                                            kind: "LinkedField",
                                            name: "user",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "publicUsername",
                                                storageKey: null
                                            }, n],
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "imageUrl",
                                            storageKey: null
                                        }, n],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "CollectionStatsType",
                                        kind: "LinkedField",
                                        name: "stats",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "totalSupply",
                                            storageKey: null
                                        }, n],
                                        storageKey: null
                                    }, n],
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }]
                    },
                    params: {
                        cacheID: "eae3939a106daa18025003116adef836",
                        id: null,
                        metadata: {},
                        name: "PartnershipPageQuery",
                        operationKind: "query",
                        text: "query PartnershipPageQuery(\n  $collections: [CollectionSlug!]\n) {\n  ...WhoPartneredWithUs_data\n}\n\nfragment AccountLink_data on AccountType {\n  address\n  config\n  isCompromised\n  user {\n    publicUsername\n    id\n  }\n  ...ProfileImage_data\n  ...wallet_accountKey\n  ...accounts_url\n}\n\nfragment CollectionCardContextMenu_data on CollectionType {\n  ...collection_url\n}\n\nfragment CollectionCard_data on CollectionType {\n  ...CollectionCardContextMenu_data\n  ...collection_url\n  description\n  name\n  shortDescription\n  slug\n  logo\n  banner\n  isVerified\n  owner {\n    ...AccountLink_data\n    id\n  }\n  stats {\n    totalSupply\n    id\n  }\n}\n\nfragment ProfileImage_data on AccountType {\n  imageUrl\n  address\n}\n\nfragment WhoPartneredWithUs_data on Query {\n  collections(first: 14, collections: $collections) {\n    edges {\n      node {\n        slug\n        ...CollectionCard_data\n        id\n      }\n    }\n  }\n}\n\nfragment accounts_url on AccountType {\n  address\n  user {\n    publicUsername\n    id\n  }\n}\n\nfragment collection_url on CollectionType {\n  slug\n}\n\nfragment wallet_accountKey on AccountType {\n  address\n}\n"
                    }
                }
            }();
            a.hash = "4a794b94ca57afbe527f211b2729e3e9", n.default = a
        },
        XUrO: function(e, n, t) {
            "use strict";
            t.d(n, "a", (function() {
                return o
            })), t.d(n, "b", (function() {
                return l
            }));
            var a = t("UutA"),
                r = t("b7Z7"),
                i = t("D4YM"),
                o = Object(a.d)(r.a).withConfig({
                    displayName: "styles__Heading",
                    componentId: "sc-ws6w85-0"
                })(["margin-bottom:32px;"]),
                l = Object(a.d)(r.a).withConfig({
                    displayName: "styles__Section",
                    componentId: "sc-ws6w85-1"
                })(["padding:40px;max-width:1280px;margin-left:auto;margin-right:auto;"]);
            Object(a.d)(r.a).withConfig({
                displayName: "styles__ColoredContainer",
                componentId: "sc-ws6w85-2"
            })(["", ""], (function(e) {
                var n = e.theme;
                return Object(i.b)({
                    variants: {
                        light: {
                            "background-color": n.colors.lightMarina
                        },
                        dark: {
                            "background-color": n.colors.onyx
                        }
                    }
                })
            }))
        },
        aQvD: function(e, n, t) {
            "use strict";
            t.r(n);
            var a, r = t("m6w3"),
                i = (t("mXGw"), t("nsHb")),
                o = t("Ojv9"),
                l = t("TGkK"),
                s = t("SMcu"),
                c = t("UutA"),
                d = t("qymy"),
                u = t("OsKK"),
                m = t("i0w7"),
                h = t("b7Z7"),
                p = t("n0tG"),
                b = t("C/iq"),
                g = t("XUrO"),
                f = t("oYCi"),
                j = function() {
                    return Object(f.jsxs)(x, {
                        children: [Object(f.jsxs)(h.a, {
                            textAlign: "center",
                            children: [Object(f.jsx)(g.a, {
                                children: Object(f.jsx)(p.a, {
                                    marginTop: 0,
                                    variant: "h2",
                                    children: "FAQs"
                                })
                            }), Object(f.jsxs)(p.a, {
                                marginBottom: 0,
                                marginX: "auto",
                                maxWidth: "700px",
                                children: ["For more FAQs visit our partnership page in our", " ", Object(f.jsx)(d.a, {
                                    href: b.S,
                                    children: "help center"
                                })]
                            })]
                        }), Object(f.jsx)(u.e, {
                            className: "FAQ--panels",
                            children: Object(f.jsxs)(u.c, {
                                children: [Object(f.jsxs)(m.a, {
                                    bodyClassName: "FAQ--panel-body",
                                    headerClassName: "FAQ--panel-header",
                                    title: "What is an NFT? What makes them so great?",
                                    children: [Object(f.jsx)(p.a, {
                                        children: "An NFT is a digital asset stored on the blockchain. These assets hold information, like descriptions, properties, and media files. NFTs are unique, one-of-a-kind, and non-interchangeable."
                                    }), Object(f.jsx)(p.a, {
                                        children: "The history of every NFT is recorded on the blockchain, meaning buyers can prove their ownership and creators can receive royalties every time their work is re-sold. NFTs can be traded on marketplaces, proudly displayed in online galleries or used to access exclusive content and real-life experiences."
                                    })]
                                }), Object(f.jsxs)(m.a, {
                                    bodyClassName: "FAQ--panel-body",
                                    headerClassName: "FAQ--panel-header",
                                    title: "What makes OpenSea special?",
                                    children: [Object(f.jsxs)(p.a, {
                                        children: ["Users can create NFTs for free at anytime across multiple blockchains. Our Polygon integration allows users to create, buy and sell NFTs without paying any transaction fee, creating a", Object(f.jsx)(d.a, {
                                            href: "/gas-free",
                                            children: " gas-free marketplace."
                                        }), " We offer the most users and have the most projects of any NFT platform."]
                                    }), Object(f.jsx)(p.a, {
                                        children: "Sellers can benefit from royalty fees of up to 10% on OpenSea. Combined with our secondary market dominance, content curation, and sorting features, your NFTs will be primed for success long after the initial sale. Combined with our secondary market dominance, content curation, and sorting features, your NFTs will be primed for success long after the initial sale."
                                    })]
                                }), Object(f.jsxs)(m.a, {
                                    bodyClassName: "FAQ--panel-body",
                                    headerClassName: "FAQ--panel-header",
                                    title: "What does a partnership with OpenSea entail?",
                                    children: [Object(f.jsx)(p.a, {
                                        children: "OpenSea partners can enjoy guidance from our industry leading experts on NFT development, sale structure, and promotion."
                                    }), Object(f.jsx)(p.a, {
                                        children: "Once you\u2019ve finalized your content, we will advise on pricing based on previous sales and current market trends. Our dedicated team will make sure you feel comfortable using the platform and advise on best practices."
                                    })]
                                }), Object(f.jsxs)(m.a, {
                                    bodyClassName: "FAQ--panel-body",
                                    headerClassName: "FAQ--panel-header",
                                    title: "What does it cost to partner with OpenSea",
                                    children: [Object(f.jsx)(p.a, {
                                        children: "OpenSea doesn't charge for partnerships or promotion. We only take our usual 2.5% on every OpenSea transaction both primary and secondary sales - the lowest in the industry."
                                    }), Object(f.jsx)(p.a, {
                                        children: "Our free API serves 1 billion requests per week, powering NFTs displayed on mobile wallets, games, art platforms, & even other marketplaces."
                                    })]
                                }), Object(f.jsx)(m.a, {
                                    bodyClassName: "FAQ--panel-body",
                                    headerClassName: "FAQ--panel-header",
                                    title: "How do we get started?",
                                    children: Object(f.jsxs)(p.a, {
                                        children: ["Currently, we\u2019re most interested in promoting projects that have multi-tiered sales (i.e. 1-of-1s, 1-of-manys, etc), have interesting & unique artwork, include robust digital unlockable content experiences, and have already been marketed in well-trafficked channels. If you fit this description, please fill in our partnership request form", " ", Object(f.jsx)(d.a, {
                                            className: "GettingStarted--link",
                                            href: b.Ib,
                                            children: "here"
                                        }), "."]
                                    })
                                })]
                            })
                        })]
                    })
                },
                x = Object(c.d)(g.b).withConfig({
                    displayName: "FAQreact__FaqSection",
                    componentId: "sc-7k82pf-0"
                })([".FAQ--panels{margin:40px 0;max-width:700px;border-top:0;border-left:0;border-right:0;border-bottom:1px solid ", ";.FAQ--panel-header{font-size:20px;}.FAQ--panel-body{padding:40px;line-height:26px;white-space:pre-wrap;}}"], (function(e) {
                    return e.theme.colors.border
                })),
                y = t("JAph"),
                O = t.n(y),
                w = function() {
                    return Object(f.jsxs)(g.b, {
                        alignItems: "center",
                        display: "flex",
                        flexDirection: {
                            _: "column",
                            xl: "row-reverse"
                        },
                        children: [Object(f.jsx)(h.a, {
                            flexShrink: 0,
                            width: {
                                xl: "50%"
                            },
                            children: Object(f.jsx)(O.a, {
                                alt: "",
                                height: "457px",
                                priority: !0,
                                src: "".concat(b.Sb, "/careers/partnerships/marketplace-illustration.svg"),
                                unoptimized: !0,
                                width: "598px"
                            })
                        }), Object(f.jsxs)(h.a, {
                            children: [Object(f.jsx)(g.a, {
                                children: Object(f.jsx)(p.a, {
                                    as: "h1",
                                    marginTop: ["revert", "revert", "revert", "revert", 0],
                                    textAlign: ["center", "center", "center", "center", "start"],
                                    variant: "h2",
                                    children: "Welcome to the world\u2019s largest NFT marketplace."
                                })
                            }), Object(f.jsx)(p.a, {
                                children: "At OpenSea, we're excited about a brand new type of digital good often referred to as a non-fungible token, or NFT. NFTs have exciting new properties: they\u2019re unique, provably scarce, liquid, and usable across multiple applications."
                            }), Object(f.jsxs)(p.a, {
                                children: ["We\u2019re proud to be the first and largest marketplace for user-owned digital goods, with everything you need to buy and sell them in one place. ", Object(f.jsx)(d.a, {
                                    href: "/about",
                                    children: " Learn more about us"
                                })]
                            })]
                        })]
                    })
                },
                v = t("aXrf"),
                C = t("GWLh"),
                k = t("Q5Gx"),
                N = t("QrBS"),
                F = t("LsOE"),
                P = function(e) {
                    var n = e.dataKey,
                        r = Object(v.useFragment)(void 0 !== a ? a : a = t("y1F7"), n),
                        i = Object(F.d)(null === r || void 0 === r ? void 0 : r.collections);
                    return null === i || void 0 === i || i.sort((function(e, n) {
                        return R[e.slug] - R[n.slug]
                    })), Object(f.jsxs)(S, {
                        textAlign: "center",
                        children: [Object(f.jsxs)(g.a, {
                            children: [Object(f.jsx)(p.a, {
                                marginTop: 0,
                                variant: "h2",
                                children: "Who has partnered with us"
                            }), Object(f.jsx)(p.a, {
                                marginBottom: 0,
                                marginX: "auto",
                                maxWidth: "700px",
                                children: "We\u2019ve collaborated with high-profile organizations and celebrities and generated millions of dollars in revenue in the process."
                            })]
                        }), Object(f.jsx)(N.a, {
                            className: "WhoPartneredWithUs--collection-cards",
                            flexWrap: "wrap",
                            children: i.map((function(e, n) {
                                return Object(f.jsx)(C.a, {
                                    dataKey: e,
                                    requireBannerImage: !0
                                }, n)
                            }))
                        })]
                    })
                },
                S = Object(c.d)(g.b).withConfig({
                    displayName: "WhoPartneredWithUsreact__WhoPartneredWithUsSection",
                    componentId: "sc-1kmj2yq-0"
                })([".WhoPartneredWithUs--collection-cards{display:grid;grid-template-columns:repeat(1,minmax(0px,1fr));gap:24px;grid-auto-rows:minmax(100px,auto);}", ""], Object(k.e)({
                    mobile: Object(c.c)([".WhoPartneredWithUs--collection-cards{display:grid;grid-template-columns:repeat(2,minmax(0px,1fr));gap:24px;grid-auto-rows:minmax(100px,auto);}"]),
                    large: Object(c.c)([".WhoPartneredWithUs--collection-cards{display:grid;grid-template-columns:repeat(3,minmax(0px,1fr));gap:24px;grid-auto-rows:minmax(100px,auto);}"])
                })),
                _ = t("7bY5"),
                K = function(e) {
                    var n = e.imageUrl,
                        t = e.title,
                        a = e.description;
                    return Object(f.jsxs)(h.a, {
                        marginX: "auto",
                        padding: "8px",
                        textAlign: "center",
                        width: ["100%", "346px"],
                        children: [Object(f.jsx)(O.a, {
                            alt: t,
                            height: 40,
                            src: n,
                            unoptimized: !0,
                            width: 40
                        }), Object(f.jsx)(p.a, {
                            variant: "h3",
                            children: t
                        }), Object(f.jsx)(p.a, {
                            children: a
                        })]
                    })
                },
                T = [{
                    imageUrl: "".concat(b.Sb, "/about/icon-money.svg"),
                    title: "Generate Revenue",
                    description: "With millions in transaction since 2017, we\u2019ll help you generate revenue on our platform \u2060\u2014 from direct sales to secondary sales"
                }, {
                    imageUrl: "".concat(b.Sb, "/about/icon-users.svg"),
                    title: "Reach Millions",
                    description: "With over thousands of users and followers, we can help you reach a large audience of collectors"
                }, {
                    imageUrl: "/static/images/icons/wallet.svg",
                    title: "Spend Less",
                    description: "Compared with other NFT marketplaces, we have lowest fees in the space allowing you to spend less with your created collection"
                }, {
                    imageUrl: "".concat(b.Sb, "/careers/partnerships/marketplace.svg"),
                    title: "Gas-free Marketplace",
                    description: "With our multiple blockchains support, create, buy, and sell NFTs without paying any gas fee"
                }, {
                    imageUrl: "/static/images/icons/collection.svg",
                    title: "Robust Features",
                    description: "With the most powerful way for users to buy and sell NFTS, we offer the most advanced features"
                }, {
                    imageUrl: "".concat(b.Sb, "/about/nfts.svg"),
                    title: "Breadth of Categories",
                    description: "With over thousands of collections, we are proud to host the widest range of categories, ranging from digital to physical NFTs"
                }];

            function W(e, n) {
                var t = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    n && (a = a.filter((function(n) {
                        return Object.getOwnPropertyDescriptor(e, n).enumerable
                    }))), t.push.apply(t, a)
                }
                return t
            }
            var A, U = function() {
                return Object(f.jsxs)(g.b, {
                    textAlign: "center",
                    children: [Object(f.jsxs)(g.a, {
                        marginBottom: 32,
                        children: [Object(f.jsx)(p.a, {
                            marginTop: 0,
                            variant: "h2",
                            children: "Why partner with OpenSea"
                        }), Object(f.jsx)(p.a, {
                            marginBottom: 0,
                            marginX: "auto",
                            maxWidth: "700px",
                            children: "While we take pride in being the first and largest marketplace and in our robust feature set, we also help our partners succeed with the following..."
                        })]
                    }), Object(f.jsx)(_.a, {
                        flexWrap: "wrap",
                        children: T.map((function(e) {
                            return Object(f.jsx)(K, function(e) {
                                for (var n = 1; n < arguments.length; n++) {
                                    var t = null != arguments[n] ? arguments[n] : {};
                                    n % 2 ? W(Object(t), !0).forEach((function(n) {
                                        Object(r.a)(e, n, t[n])
                                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : W(Object(t)).forEach((function(n) {
                                        Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n))
                                    }))
                                }
                                return e
                            }({}, e), e.title)
                        }))
                    })]
                })
            };

            function B(e, n) {
                var t = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    n && (a = a.filter((function(n) {
                        return Object.getOwnPropertyDescriptor(e, n).enumerable
                    }))), t.push.apply(t, a)
                }
                return t
            }

            function Q(e) {
                for (var n = 1; n < arguments.length; n++) {
                    var t = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? B(Object(t), !0).forEach((function(n) {
                        Object(r.a)(e, n, t[n])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : B(Object(t)).forEach((function(n) {
                        Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n))
                    }))
                }
                return e
            }
            var I = ["888-by-kevin-abosch", "the-mike-tyson-nft-collection-by-cory-van-lew", "delorean-s-40th-anniversary-nft-collection", "reddit-cryptosnoos", "pacmanxgenies", "golden-state-warriors-legacy-collection", "lil-jon-got-an-nft-btch", "saturday-night-live", "simco-drops-petra-cortright-room", "rob-gronkowski-championship-series-nfts", "genies", "coca-cola"],
                R = I.reduce((function(e, n, t) {
                    return Q(Q({}, e), {}, Object(r.a)({}, n, t))
                }), {}),
                D = function(e) {
                    var n = e.data;
                    return Object(f.jsxs)(l.a, {
                        title: Object(s.b)("Partners"),
                        children: [Object(f.jsx)(w, {}), Object(f.jsx)(U, {}), Object(f.jsx)(P, {
                            dataKey: null !== n && void 0 !== n ? n : null
                        }), Object(f.jsx)(j, {}), Object(f.jsx)(i.a, {}), Object(f.jsx)(o.a, {})]
                    })
                };
            D.query = void 0 !== A ? A : A = t("Ws2v"), D.getInitialProps = function() {
                return {
                    variables: {
                        collections: I
                    }
                }
            };
            n.default = D
        },
        bXFq: function(e, n, t) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/partners", function() {
                return t("aQvD")
            }])
        },
        bmVz: function(e, n, t) {
            "use strict";
            t.r(n);
            var a = function() {
                var e = {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "slug",
                    storageKey: null
                };
                return {
                    argumentDefinitions: [],
                    kind: "Fragment",
                    metadata: null,
                    name: "CollectionCard_data",
                    selections: [{
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "description",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "name",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "shortDescription",
                        storageKey: null
                    }, e, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "logo",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "banner",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isVerified",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        concreteType: "AccountType",
                        kind: "LinkedField",
                        name: "owner",
                        plural: !1,
                        selections: [{
                            args: null,
                            kind: "FragmentSpread",
                            name: "AccountLink_data"
                        }],
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        concreteType: "CollectionStatsType",
                        kind: "LinkedField",
                        name: "stats",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "totalSupply",
                            storageKey: null
                        }],
                        storageKey: null
                    }, {
                        args: null,
                        kind: "FragmentSpread",
                        name: "CollectionCardContextMenu_data"
                    }, {
                        kind: "InlineDataFragmentSpread",
                        name: "collection_url",
                        selections: [e]
                    }],
                    type: "CollectionType",
                    abstractKey: null
                }
            }();
            a.hash = "a7d75a2214330a37ef349a380417086d", n.default = a
        },
        hc1Z: function(e, n, t) {
            "use strict";
            t.r(n);
            var a = {
                argumentDefinitions: [],
                kind: "Fragment",
                metadata: null,
                name: "CollectionCardContextMenu_data",
                selections: [{
                    kind: "InlineDataFragmentSpread",
                    name: "collection_url",
                    selections: [{
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "slug",
                        storageKey: null
                    }]
                }],
                type: "CollectionType",
                abstractKey: null,
                hash: "97b1d12017f765d3122871168f060d38"
            };
            n.default = a
        },
        i0w7: function(e, n, t) {
            "use strict";
            t.d(n, "a", (function() {
                return v
            }));
            var a = t("mXGw"),
                r = t.n(a),
                i = t("8Jek"),
                o = t.n(i),
                l = t("UutA"),
                s = t("m5he"),
                c = t("etRO"),
                d = t("4jfz"),
                u = t("g2+O"),
                m = t("mHfP"),
                h = t("1U+3"),
                p = t("DY1Z"),
                b = t("m6w3"),
                g = t("t3V9"),
                f = t("u6YR"),
                j = t("oYCi");

            function x(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var t, a = Object(p.a)(e);
                    if (n) {
                        var r = Object(p.a)(this).constructor;
                        t = Reflect.construct(a, arguments, r)
                    } else t = a.apply(this, arguments);
                    return Object(h.a)(this, t)
                }
            }
            var y = function(e) {
                    Object(m.a)(t, e);
                    var n = x(t);

                    function t() {
                        var e;
                        Object(c.a)(this, t);
                        for (var a = arguments.length, r = new Array(a), i = 0; i < a; i++) r[i] = arguments[i];
                        return e = n.call.apply(n, [this].concat(r)), Object(b.a)(Object(u.a)(e), "renderHeader", (function(e) {
                            var n = e.children,
                                t = e.className,
                                a = e.isDisabled,
                                r = e.onClick;
                            return Object(j.jsx)(g.a, {
                                className: Object(f.a)("BasePanel--header", {
                                    isDisabled: a
                                }, o()("BasePanel--header", t)),
                                onClick: r,
                                children: n
                            })
                        })), Object(b.a)(Object(u.a)(e), "renderBody", (function(e) {
                            var n = e.children,
                                t = e.className;
                            return Object(j.jsx)("div", {
                                className: o()("BasePanel--body", t),
                                children: n
                            })
                        })), Object(b.a)(Object(u.a)(e), "renderFooter", (function(e) {
                            var n = e.children,
                                t = e.className;
                            return Object(j.jsx)("div", {
                                className: o()("BasePanel--footer", t),
                                children: n
                            })
                        })), e
                    }
                    return Object(d.a)(t, [{
                        key: "render",
                        value: function() {
                            var e = this.props,
                                n = e.className,
                                t = e.children;
                            return Object(j.jsx)(O, {
                                className: n,
                                children: t({
                                    Header: this.renderHeader,
                                    Body: this.renderBody,
                                    Footer: this.renderFooter
                                })
                            })
                        }
                    }]), t
                }(r.a.Component),
                O = l.d.div.withConfig({
                    displayName: "BasePanelreact__DivContainer",
                    componentId: "sc-1d6z6bk-0"
                })(["border-radius:", ";border:1px solid ", ";border-top:none;color:", ";background-color:", ";overflow:hidden;.BasePanel--header{width:100%;align-items:center;border-radius:", ";border-top:1px solid ", ";cursor:pointer;display:flex;font-size:16px;font-weight:600;padding:20px;user-select:none;background-color:", ";&.BasePanel--header--isDisabled{cursor:initial;color:", ";}}.BasePanel--body{border-top:1px solid ", ";color:", ";background-color:", ";}.BasePanel--footer{border-top:1px solid ", ";color:", ";background-color:", ";}"], (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.text.heading
                }), (function(e) {
                    return e.theme.colors.header
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.header
                }), (function(e) {
                    return e.theme.colors.text.subtle
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.text.body
                }), (function(e) {
                    return e.theme.colors.surface
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.text.body
                }), (function(e) {
                    return e.theme.colors.header
                })),
                w = t("OsKK"),
                v = function(e) {
                    var n = e.bodyClassName,
                        t = e.children,
                        i = e.className,
                        l = e.headerClassName,
                        c = e.footerClassName,
                        d = e.FooterButton,
                        u = e.icon,
                        m = e.iconTheme,
                        h = e.iconColor,
                        p = e.isContentPadded,
                        b = void 0 === p || p,
                        g = e.maxHeight,
                        x = e.mode,
                        O = void 0 === x ? "start-closed" : x,
                        v = e.open,
                        k = e.onClick,
                        N = e.title,
                        F = e.variant,
                        P = void 0 === F ? "default" : F,
                        S = Object(a.useState)(0),
                        _ = S[0],
                        K = S[1],
                        T = Object(a.useState)((function() {
                            return !!r.a.Children.count(t) && ("start-open" === O || "always-open" === O)
                        })),
                        W = T[0],
                        A = T[1],
                        U = !r.a.Children.count(t),
                        B = "controlled" === O,
                        Q = B ? v : W,
                        I = Object(a.useRef)(null),
                        R = function() {
                            B ? null === k || void 0 === k || k(!v) : r.a.Children.count(t) && "always-open" !== O && A(!W)
                        };
                    return Object(a.useEffect)((function() {
                        var e;
                        return _ && (e = window.setTimeout((function() {
                                K(void 0)
                            }), 100)),
                            function() {
                                e && clearTimeout(e)
                            }
                    }), [_]), Object(a.useEffect)((function() {
                        var e;
                        K(null === (e = I.current) || void 0 === e ? void 0 : e.getBoundingClientRect().height)
                    }), [W]), Object(j.jsx)(w.b, {
                        children: function(e) {
                            var a = e.isFramed;
                            return Object(j.jsx)(C, {
                                className: Object(f.a)("Panel", {
                                    alwaysOpen: "always-open" === O,
                                    isOpen: Q,
                                    isClosed: !Q,
                                    isFramed: a,
                                    warning: "warning" === P
                                }, i),
                                "data-testid": "Panel",
                                children: Object(j.jsx)(y, {
                                    className: "Panel--panel",
                                    children: function(e) {
                                        var a = e.Header,
                                            r = e.Body,
                                            i = e.Footer;
                                        return Object(j.jsxs)(j.Fragment, {
                                            children: [Object(j.jsxs)(a, {
                                                className: o()("Panel--header", l),
                                                isDisabled: U,
                                                onClick: R,
                                                children: [u && Object(j.jsx)(s.a, {
                                                    className: "Panel--icon",
                                                    color: h,
                                                    value: u,
                                                    variant: m
                                                }), "string" === typeof N ? Object(j.jsx)("span", {
                                                    children: N
                                                }) : N, Object(j.jsx)(s.a, {
                                                    className: Object(f.a)("Panel", {
                                                        toggle: !0,
                                                        isEnabled: !U
                                                    }),
                                                    value: Q ? "expand_less" : "expand_more",
                                                    variant: m
                                                })]
                                            }), Object(j.jsx)(r, {
                                                className: Object(f.a)("Panel", {
                                                    body: !0,
                                                    "body-warning": "warning" === P,
                                                    "body-is-closed": !Q
                                                }),
                                                children: Object(j.jsx)("div", {
                                                    className: "Panel--content-container",
                                                    ref: I,
                                                    style: {
                                                        height: Q ? _ || "initial" : 0,
                                                        maxHeight: g,
                                                        overflow: g ? "auto" : void 0
                                                    },
                                                    children: Object(j.jsx)("div", {
                                                        className: Object(f.a)("Panel", {
                                                            isContentPadded: b
                                                        }, n),
                                                        children: t
                                                    })
                                                })
                                            }), Q && d && Object(j.jsx)(i, {
                                                className: o()("Panel--footer", c),
                                                children: d
                                            })]
                                        })
                                    }
                                })
                            })
                        }
                    })
                },
                C = l.d.div.withConfig({
                    displayName: "Panelreact__DivContainer",
                    componentId: "sc-1uztusg-0"
                })(["&.Panel--isOpen{.Panel--content-container{overflow:visible;}}&.Panel--isClosed{.Panel--header{margin-bottom:2px;}.Panel--body{border:none;&.Panel--body-is-closed{display:none;}}}&.Panel--isFramed{.Panel--panel,.Panel--header,.Panel--body{border-radius:0;}.Panel--panel{border-top:none;border-left:none;border-right:none;margin-bottom:-1px;margin-top:1px;}.Panel--header{border:0;margin:0;}.Panel--body{border-left:0;border-right:0;}}.Panel--icon{margin-right:10px;}.Panel--toggle{margin-left:auto;color:", ";}.Panel--header{&:hover{.Panel--toggle.Panel--isEnabled{color:", ";}}.Panel--header-text{display:flex;align-items:center;}}.Panel--content-container{overflow:hidden;transition:height ", "ms;}.Panel--isContentPadded{padding:20px;border-bottom-left-radius:", ";border-bottom-right-radius:", ";}&.Panel--alwaysOpen{.Panel--header{cursor:initial;.Panel--toggle{display:none;}}}&.Panel--warning{border-radius:", ";.Panel--panel{border-bottom:none;}&.Panel--isClosed{border-bottom:1px solid ", ";.Panel--body-warning{border:none;}.Panel--header{border-radius:", ";}}.Panel--body-warning{background-color:", ";border:1px solid ", ";}.Panel--header{background-color:", ";border-color:", ";border-radius:inherit inherit 0px 0px;margin-bottom:0px;padding:20px 15px;}}"], (function(e) {
                    return e.theme.colors.withOpacity.text.heading.medium
                }), (function(e) {
                    return e.theme.colors.text.heading
                }), 100, (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.warning
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.withOpacity.warning.veryLight
                }), (function(e) {
                    return e.theme.colors.warning
                }), (function(e) {
                    return e.theme.colors.withOpacity.warning.veryLight
                }), (function(e) {
                    return e.theme.colors.warning
                }))
        },
        nsHb: function(e, n, t) {
            "use strict";
            t.d(n, "a", (function() {
                return f
            }));
            var a = t("uEoR"),
                r = t("mXGw"),
                i = t("JAph"),
                o = t.n(i),
                l = t("UutA"),
                s = t("Q5Gx"),
                c = t("67yl"),
                d = t("QrBS"),
                u = t("n0tG"),
                m = t("eV01"),
                h = t("C/iq"),
                p = t("XUrO"),
                b = t("oYCi"),
                g = function() {
                    var e = Object(r.useRef)(null),
                        n = Object(m.a)(e),
                        t = Object(a.a)(n, 1)[0],
                        i = Math.min(435, t);
                    return Object(b.jsx)(c.a, {
                        maxWidth: "450px",
                        ref: e,
                        width: "100%",
                        children: Object(b.jsx)(o.a, {
                            alt: "Katie Haun",
                            height: .8689655172413793 * i,
                            src: "".concat(h.Sb, "/careers/katie-haun.png"),
                            unoptimized: !0,
                            width: i
                        })
                    })
                },
                f = function() {
                    return Object(b.jsx)(j, {
                        children: Object(b.jsx)(d.a, {
                            className: "KatieHaun--container",
                            children: Object(b.jsxs)(d.a, {
                                className: "KatieHaun--sided",
                                children: [Object(b.jsxs)(d.a, {
                                    className: "KatieHaun--sided-text",
                                    children: [Object(b.jsx)(d.a, {
                                        className: "KatieHaun--quote-image-container",
                                        children: Object(b.jsx)(o.a, {
                                            alt: "Quote",
                                            height: 32,
                                            src: "/static/images/drawings/quote.svg",
                                            unoptimized: !0,
                                            width: 32
                                        })
                                    }), Object(b.jsx)(u.a, {
                                        className: "KatieHaun--katie-quote",
                                        fontSize: "20px",
                                        variant: "bold",
                                        children: "OpenSea is one of the most exciting, important companies in the world right now because it's the portal to the new digital economy. If you're interested in shaping a new business model for creators, this is the team to join."
                                    }), Object(b.jsx)(u.a, {
                                        margin: "0",
                                        variant: "bold",
                                        children: "Katie Haun"
                                    }), Object(b.jsx)(u.a, {
                                        margin: "0",
                                        children: "General Partner at Andreessen Horowitz"
                                    })]
                                }), Object(b.jsx)(g, {})]
                            })
                        })
                    })
                },
                j = Object(l.d)(p.b).withConfig({
                    displayName: "KatieHaunreact__DivContainer",
                    componentId: "sc-15iuira-0"
                })([".KatieHaun--container{flex-direction:column;align-items:center;.KatieHaun--sided{align-items:center;justify-content:space-evenly;margin-bottom:60px;flex-direction:column;max-width:100%;", " .KatieHaun--sided-text{flex-direction:column;order:1;margin-top:0px;text-align:center;max-width:720px;", ";.KatieHaun--quote-image-container{width:100%;justify-content:center;margin-bottom:30px;", "}.KatieHaun--katie-quote{margin-top:0;line-height:28px;}}}}"], Object(s.e)({
                    extraLarge: Object(l.c)(["flex-direction:row;max-width:", ";"], (function(e) {
                        return e.theme.maxWidth.smallPadding
                    }))
                }), Object(s.e)({
                    extraLarge: Object(l.c)(["width:45%;order:0;text-align:left;"])
                }), Object(s.e)({
                    tabletS: Object(l.c)(["justify-content:flex-start;"])
                }))
        },
        xiTr: function(e, n, t) {
            "use strict";
            t("mXGw");
            var a = t("UutA"),
                r = t("b7Z7"),
                i = t("QrBS"),
                o = t("u6YR"),
                l = t("uMSw"),
                s = t("qymy"),
                c = t("oYCi");
            n.a = function(e) {
                var n = e.imageUrl,
                    t = e.href,
                    a = e.imageWidth,
                    r = e.imageHeight,
                    u = e.containerClassName,
                    m = e.contentClassName,
                    h = e.className,
                    p = e.children,
                    b = e.eventSource,
                    g = e.alt;
                return Object(c.jsx)(d, {
                    className: u,
                    children: Object(c.jsxs)(s.a, {
                        className: Object(o.a)("CarouselCard", {
                            main: !0
                        }, h),
                        eventSource: b,
                        href: t,
                        children: [Object(c.jsx)(l.a, {
                            alt: g,
                            className: "CarouselCard--image",
                            height: r,
                            sizing: "cover",
                            url: n,
                            width: a
                        }), Object(c.jsx)(i.a, {
                            className: Object(o.a)("CarouselCard", {
                                content: !0
                            }, m),
                            children: p
                        })]
                    })
                })
            };
            var d = Object(a.d)(r.a).withConfig({
                displayName: "CarouselCardreact__Container",
                componentId: "sc-152cap8-0"
            })(["display:inline-block;width:100%;.CarouselCard--main{display:inline-block;border:1px solid ", ";background-color:", ";border-radius:", ";cursor:pointer;width:100%;&:hover{box-shadow:", ";transition:0.1s;}.CarouselCard--image{border-radius:inherit;border-bottom-left-radius:0;border-bottom-right-radius:0;border-bottom:1px solid ", ";background-color:", ";}.CarouselCard--content{flex-direction:column;padding:10px;}}"], (function(e) {
                return e.theme.colors.border
            }), (function(e) {
                return e.theme.colors.card
            }), (function(e) {
                return e.theme.borderRadius.default
            }), (function(e) {
                return e.theme.shadows.default
            }), (function(e) {
                return e.theme.colors.border
            }), (function(e) {
                return e.theme.colors.border
            }))
        },
        y1F7: function(e, n, t) {
            "use strict";
            t.r(n);
            var a = {
                argumentDefinitions: [{
                    kind: "RootArgument",
                    name: "collections"
                }],
                kind: "Fragment",
                metadata: null,
                name: "WhoPartneredWithUs_data",
                selections: [{
                    alias: null,
                    args: [{
                        kind: "Variable",
                        name: "collections",
                        variableName: "collections"
                    }, {
                        kind: "Literal",
                        name: "first",
                        value: 14
                    }],
                    concreteType: "CollectionTypeConnection",
                    kind: "LinkedField",
                    name: "collections",
                    plural: !1,
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "CollectionTypeEdge",
                        kind: "LinkedField",
                        name: "edges",
                        plural: !0,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "CollectionType",
                            kind: "LinkedField",
                            name: "node",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "slug",
                                storageKey: null
                            }, {
                                args: null,
                                kind: "FragmentSpread",
                                name: "CollectionCard_data"
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    storageKey: null
                }],
                type: "Query",
                abstractKey: null,
                hash: "ce07d1b5e26fbaa7555c9e1849da4216"
            };
            n.default = a
        }
    },
    [
        [44, 2, 1, 6, 4, 3, 7, 9, 0, 5, 8, 11, 13, 28]
    ]
]);
//# sourceMappingURL=partners-1b8fa1c2c9882ab9273f.js.map